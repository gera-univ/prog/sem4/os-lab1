#include <stdio.h>
#include <stdlib.h>

#include "common.h"

void print_usage(char* argv0, int exit_code) {
	printf("%s output_file number_of_entries", argv0);
	exit(exit_code);
}

struct employee* read_entries(long int number_of_entries) {
	if (number_of_entries == 0)
		return NULL;
	struct employee* entries = malloc(number_of_entries * sizeof(struct employee));
	if (entries != NULL) {
		for (long int i = 0; i < number_of_entries; ++i) {
			scanf_s("%s %lf", &entries[i].name, EMPLOYEE_MAX_NAME, &entries[i].hours);
			entries[i].num = i + 1;
		}
	}
	return entries;
}

void write_entries(FILE* ptr_output_file, struct employee* entries, long int number_of_entries) {
	fwrite(entries, sizeof(struct employee), number_of_entries, ptr_output_file);
}

int main(int argc, char** argv) {
	if (argc < 3)
		print_usage(argv[0], 0);

#define output_file_name argv[1]
	
	const long int number_of_entries = strtol(argv[2], NULL, 10);

	printf("Reading %ld entries:\nname\thours\n", number_of_entries);
	struct employee* entries = read_entries(number_of_entries);
	if (entries == NULL) {
		fprintf(stderr, "Couldn't allocate memory");
	}

	FILE* ptr_output_file = NULL;
	fopen_s(&ptr_output_file, output_file_name, "w");
	if (ptr_output_file == NULL) {
		fprintf(stderr, "Couldn't open file: %s", output_file_name);
		return 1;
	}

	write_entries(ptr_output_file, entries, number_of_entries);

	free(entries);
	fclose(ptr_output_file);

	return 0;
}
