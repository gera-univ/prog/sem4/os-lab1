#include <stdio.h>
#include <stdlib.h>

#include "common.h"

void print_usage(char* argv0, int exit_code) {
	printf("%s input_file report_file hourly_rate", argv0);
	exit(exit_code);
}

struct employee* read_entries_file(FILE* ptr_file, long *ret_number_of_entries) {
	fseek(ptr_file, 0L, SEEK_END);
	*ret_number_of_entries = ftell(ptr_file) / (long)sizeof(struct employee);
	rewind(ptr_file);
	struct employee* entries = malloc(*ret_number_of_entries * sizeof(struct employee));
	if (entries != NULL) {
		fread(entries, sizeof(struct employee), *ret_number_of_entries, ptr_file);	
	}
	return entries;
}

void print_entries(struct employee* entries, long number_of_entries) {
	for (long i = 0; i < number_of_entries; i++)	{
		printf("%d %s %g\n", entries[i].num, entries[i].name, entries[i].hours);
	}
}

void write_report_to_file(struct employee* entries, long number_of_entries, double hourly_rate, FILE* report_file, const char *input_file_name) {
	fprintf(report_file, "Report based on data from %s\nHourly rate: %g\n", input_file_name, hourly_rate);
	fprintf(report_file, "num\tname\thours\tsalary\n");
	for (long i = 0; i < number_of_entries; i++)	{
		fprintf(report_file, "%d\t%s\t%g\t%g\n", entries[i].num, entries[i].name, entries[i].hours, entries[i].hours * hourly_rate);
	}
}

int main(int argc, char** argv) {
	if (argc < 4)
		print_usage(argv[0], 0);

#define input_file_name argv[1]
#define report_file_name argv[2]

	double hourly_rate = strtod(argv[3], NULL);
	
	FILE* ptr_input_file = NULL;
	fopen_s(&ptr_input_file, input_file_name, "r");
	if (ptr_input_file == NULL) {
		fprintf(stderr, "Couldn't open input file: %s", input_file_name);
		return 1;
	}

	long number_of_entries;
	struct employee* entries = read_entries_file(ptr_input_file, &number_of_entries);

	printf("Read %ld entries\n", number_of_entries);
	print_entries(entries, number_of_entries);

	FILE* ptr_report_file = NULL;
	fopen_s(&ptr_report_file, report_file_name, "w");
	if (ptr_report_file == NULL) {
		fprintf(stderr, "Couldn't open report file: %s", report_file_name);
		return 1;
	}

	write_report_to_file(entries, number_of_entries, hourly_rate, ptr_report_file, input_file_name);
	printf("Report has been written to file %s\n", report_file_name);

	free(entries);
	fclose(ptr_input_file);
	fclose(ptr_report_file);

	return 0;
}