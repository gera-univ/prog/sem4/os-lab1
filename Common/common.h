#pragma once

#define EMPLOYEE_MAX_NAME 10

struct employee {
	int num;
	char name[EMPLOYEE_MAX_NAME];
	double hours;
};
