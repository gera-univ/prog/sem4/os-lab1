#include <stdio.h>
#include <stdlib.h>
#include <wchar.h>
#include <windows.h>

void print_usage(char* argv0, int exit_code) {
	printf("%s input_file number_of_entries report_file hourly_rate", argv0);
	exit(exit_code);
}

void spawn_process(char format_string[], ...) {
	va_list va;
	va_start(va, format_string);
	char arg_string[MAX_PATH];
	vsprintf_s(arg_string, MAX_PATH, format_string, va);
	va_end(va);

	wchar_t arg_string_wide[MAX_PATH];

	STARTUPINFO si;
	PROCESS_INFORMATION piCom;

	ZeroMemory(&si, sizeof(STARTUPINFO));
	si.cb = sizeof(STARTUPINFO);

	size_t num_converted;
	mbstowcs_s(&num_converted, arg_string_wide, MAX_PATH, arg_string, MAX_PATH);

	BOOL success = CreateProcess(NULL, arg_string_wide, NULL, NULL, FALSE,
	                             CREATE_NEW_CONSOLE, NULL, NULL, &si, &piCom);
	if (!success)
		fprintf(stderr, "Unable to launch process:\n  %s\n", arg_string);

	WaitForSingleObject(piCom.hProcess, INFINITE);

	CloseHandle(piCom.hThread);
	CloseHandle(piCom.hProcess);
}

void print_file_contents(FILE *ptr_file) {
	char ch;
	do {
		ch = fgetc(ptr_file);
		putchar(ch);
	}
	while (ch != EOF);
}

int main(int argc, char** argv) {
	if (argc < 4)
		print_usage(argv[0], 0);

#define input_file_name argv[1]
#define report_file_name argv[3]

	const long int number_of_entries = strtol(argv[2], NULL, 10);
	double hourly_rate = strtod(argv[4], NULL);


	printf("Starting creator...\n");
	spawn_process("Creator.exe %s %ld", input_file_name, number_of_entries);
	printf("Starting reporter...\n");
	spawn_process("Reporter.exe %s %s %lf", input_file_name, report_file_name, hourly_rate);

	printf("Reading report file...\n");
	
	FILE* ptr_report_file = NULL;
	fopen_s(&ptr_report_file, report_file_name, "r");
	if (ptr_report_file == NULL) {
		fprintf(stderr, "Couldn't open report file: %s", report_file_name);
		return 1;
	}

	print_file_contents(ptr_report_file);

	fclose(ptr_report_file);

	return 0;
}
